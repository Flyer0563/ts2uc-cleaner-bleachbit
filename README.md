## Before we start..
This was made for The Sims 2: Starter Pack and The Sims 2: Ultimate Collection (Origin/EA App) in mind. check [Requirements](#requirements) for more detail as well as some [issue](https://gitlab.com/Flyer0563/ts2uc-cleaner-bleachbit/-/wikis/Issue) you may encounter
## About
Trying to setup The Sims 2 and make it sightly cleaner can be a tedious task and I often even skip them. Thanks to BleachBit, it makes cleaning the game file less of a chore and more automated.
All it does is clean up and delete some files from the directory folder of the game, not edit the ``.package`` file in anyway. This isn't a replacement for simPE at all!

Check the [Wiki](https://gitlab.com/Flyer0563/ts2uc-cleaner-bleachbit/-/wikis/home) for more details

### Features
- Remove Archive Files in Download folder such as `.zip, .rar, .7z`
- Clear Logs folder
- Removes the The Sims 2 Intro from startup
- Removes [Stealth Neighbourhood](https://sims.fandom.com/wiki/Secret_sub-neighborhood) from generating on new Neighbourhood
### What doesn't it do..
- Removes Stealth Neighbourhood on current Neighbourhood that's already been affected.
	- It is due to how the game works where even if your Sims died, there's some files remain which is relevant to have to have things like Gossip to work without it crashing or breaking the game.
    - Removes an EP/SP from Ultimate Collection
    	- Deleting EP/SP can break your game
        - [AnyGameStarter](https://modthesims.info/d/604873/) is a much better choice
    - Removes any token on Sims
        - This project is to clean up game folder ONLY! Use [SimPE](https://simfileshare.net/folder/77372/) if you want to tweak anything for that specific Sim.
## Setting up
### Requirements
- [BleachBit](https://www.bleachbit.org/)
- Any of this following copy of The Sims 2...
    - [The Sims 2 Starter Pack](https://github.com/voicemxil/TS2-Starter-Pack)
    - The Sims 2 Ultimate Collection (EA App/Origin)
        > MrDJ and OldGamesDownload will not be 100% compatible. Doing cleanup such as Remove intro video and Stealth Neighbourhood will not work. 
        >
        > Please use The Sims 2 Starter Pack instead which not only it was built for that in mind but also it preinstall some mods/tools such as GraphicsRulesMaker and SimShadowFix
- Windows (Linux and MacOS support will come soon)

Insert the files to your BleachBit Install folder `~\BleachBit\share\cleaners`
| Filename | Version |
| ------ | ------ |
|`sims2_starterpack.xml`|The Sims 2: Starter Pack|
|`sims2UC_Origin.xml`|The Sims 2: Ultimate Collection (Origin)|
`sims2UC_EAapp.xml` |The Sims 2: Ultimate Collection (EA App)|

